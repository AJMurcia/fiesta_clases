class Asistente{
    
    constructor(name, lluviatrago){
        this.name=name;
        this.lluviatrago=lluviatrago;
    }

    get comer(){
        let acto2=(this.name+" come pollo asado con sus amigas Lore y Angie");
        return acto2;
    }
    get comprarcomida(){

    }
    get invitartragoaguardiente(){
        let acto16=(this.name+" invita un aguardiente a Dilan");
            return acto16;
    }
    get invitartragocerveza(){
       
            let acto6=(this.name+" invita una cerveza a Juanita");
            return acto6;
      
    }
    get emborracharse(){
        let acto23=(this.name+" se emborracha");
        return acto23;
    }
    get invitarbailar(){
            let acto10=(this.name+" invita a bailar a Juanita");
            return acto10;
    }
    get bailar(){
        let acto18=(this.name+" baila con Angie y Lore al tiempo, se fue con todo");
        return acto18;

    }
    get bebervaso(){
        let acto4=(this.name+" bebe un vaso de cerveza");
        return acto4;
    }
    get vomitar(){
        let acto25=(this.name+" vomita los cables del equipo de sonido");
        return acto25;
    }
    get coquetear(){
        let acto21=(this.name+" coquetea con Juanita");
        return acto21;
    }
    get besar(){
        let acto24=(this.name+" besa a Dilan");
        return acto24;
    }
    get hablar(){
            let acto1=(this.name+" esta hablando con Miguel sobre Juanita");
            return acto1;
        }
    get saludar(){
            let acto22=(this.name+" trata de saludar a Lore pero lo ignora");
            return acto22;            
    }
    get ponermusica(){
        let acto20=(this.name+" coloca bachata");
        return acto20;
    }
}
class Trago{
    constructor(marca,cantidad,gradoalcohol){
        this.marca=marca;
        this.cantidad=cantidad;
        this.gradoalcohol=gradoalcohol;
    }
    get serconcumido(){
        let acto12=(this.marca+" se acabo, culpa de Federico");
        return acto12;
    }
    get tomarcantdeseada(){
        let acto13=("Federico toma un vaso de "+this.marca);
        return acto13;
    }
}
class Musica{
    constructor(genero,tiempo){
        this.genero=genero;
        this.tiempo=tiempo;
    }
    get reproducirse(){
            let acto8=("Miguel pone "+this.genero+" por "+this.tiempo);
            return acto8;
    }
    get apagarse(){
        let acto26=("la musica se apagó cuando sonaba "+this.genero);
        return acto26;
    }
    get serbailada(){

    }   
    get cambiarse(){
        let acto14=("Miguel coloca "+this.genero+" por "+this.tiempo);
        return acto14; 
    }
}
class Comida{
    constructor(tipo,precio,cantidad){
        this.tipo=tipo;
        this.precio=precio;
        this.cantidad=cantidad;
    }
    get serconsumida(){
        let acto5=(this.tipo+" se acabo");
        return acto5;
    }
    get sercomprada(){
        let acto3=(this.tipo+" fueron compradas por Federico");
        return acto3;
    }
}